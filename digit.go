/*
Grovel through a json blob represented as a series of nested
map[string]interface{} objects.

Usage:

	grovel path/to/target

	reads from stdin, writes to stdout

For exqmple:

	cat json | grovel path/to/target

*/

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
	"strings"
)

func main() {

	var (
		at string
		blob map[string]interface{}
		body []byte
		elem string
		elems []string
		err error
		found string
		ii int
		ok bool
		rmsg interface{}
	)

	if !args() {
		fmt.Fprintf(os.Stderr, "FATAL: bad args\n")
		return
	}

	body, err = ioutil.ReadAll(os.Stdin)
	if err != nil {
		fmt.Fprintf(os.Stderr, "FATAL: failed to read json: %s\n", err)
		return
	}

	err = json.Unmarshal(body, &blob)
	if err != nil {
		fmt.Fprintf(os.Stderr, "FATAL: failed to unmarshal: %s\n", err)
		return
	}

	elems = strings.Split(os.Args[1], "/")
	at = ""
loop:
	for ii = 0; ii < len(elems); ii++ {

		elem = elems[ii]
		at += elem + "/"

		if rmsg, ok = blob[elem]; !ok {
			fmt.Fprintf(os.Stderr, "no path to %s\n", elem)
			return
		}

		if kk := reflect.TypeOf(rmsg).Kind(); kk == reflect.Map {
			blob = rmsg.(map[string]interface{})
		} else {
			switch kk {
			case reflect.String:
				found = fmt.Sprintf("%s", reflect.ValueOf(rmsg))
			case reflect.Bool:
				found = fmt.Sprintf("%t", reflect.ValueOf(rmsg))
			case reflect.Float64:
				found = fmt.Sprintf("%f", reflect.ValueOf(rmsg))
			default:
				fmt.Printf("BUG: unexpected type: %s\n", reflect.TypeOf(rmsg))
			}
			break loop
		}
	}

	if ii == len(elems) {
		// left loop with non-string value
		fmt.Printf("%s: structured value\n", at)
	} else if ii == len(elems) - 1 {
		// exited at last path element and with string value
		fmt.Printf("%s: %s\n", at, found)
	} else {
		// exited too soon, no path
		fmt.Printf("%s not a path\n", os.Args[1])
	}
}

func args() bool {
	return len(os.Args) > 1
}


